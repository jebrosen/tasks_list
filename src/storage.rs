use std::fs::File;
use std::io;
use std::path::Path;

use r2d2;
use r2d2_sqlite::SqliteConnectionManager;
use rusqlite;
use rusqlite::Row;
use rusqlite::Error::QueryReturnedNoRows;
use serde_json;

use task::*;

pub trait TaskStorage {
    fn get_task(&self, id: u32) -> Option<Task>;
    fn insert_task(&mut self, task: NewTask);
    fn put_task(&mut self, task: Task);
    fn delete_task(&mut self, id: u32) -> Option<Task>;
    fn get_tasks(&self) -> Vec<Task>;
    fn get_tasks_by_tag(&self, tag: &str) -> Vec<Task>;
}

#[derive(Serialize, Deserialize)]
struct TasksList {
    pub tasks: Vec<Task>,
}

pub fn load<P: AsRef<Path>>(path: P) -> Result<Vec<Task>, io::Error> {
    let f = File::open(path)?;
    let tl: TasksList = serde_json::from_reader(f)?;
    Ok(tl.tasks)
}

pub fn save<P: AsRef<Path>>(path: P, tasks: Vec<Task>) -> Result<(), io::Error> {
    let f = File::create(path)?;
    let tl = TasksList { tasks: tasks };

    serde_json::to_writer_pretty(f, &tl)?;
    Ok(())
}

pub struct DbPool(r2d2::Pool<SqliteConnectionManager>);

pub struct DbConn(r2d2::PooledConnection<SqliteConnectionManager>);

impl DbPool {
    pub fn new<P: AsRef<Path>>(path: P) -> Result<Self, r2d2::Error> {
        let pool = DbPool(r2d2::Pool::new(SqliteConnectionManager::file(path))?);
        let conn = pool.connect()?;
        conn.0.execute_batch(SCHEMA).expect("create tables");
        Ok(pool)
    }

    pub fn connect(&self) -> Result<DbConn, r2d2::Error> {
        self.0.get().map(DbConn)
    }
}

impl DbConn {
    fn task_from_row(row: &Row) -> Task {
        Task::new(
            row.get("id"),
            row.get("name"),
            vec![],
            row.get("due"),
            row.get::<&str, f64>("hours") as f32,
        )
    }

    fn tags_for_task(&self, id: u32) -> Vec<String> {
        let mut stmt = self.0
            .prepare("SELECT tag from tasks_tags WHERE task = ?;")
            .expect("query tags statement");
        let tags: Result<Vec<String>, rusqlite::Error> =
            stmt.query_map(&[&id], |row| row.get(0)).unwrap().collect();
        tags.expect("query tags results")
    }

    fn delete_task_internal(tx: &rusqlite::Transaction, id: u32) {
        tx.execute("DELETE FROM tasks_tags WHERE task = ?;", &[&id])
            .expect("delete task tags");
        tx.execute("DELETE FROM tasks WHERE id = ?;", &[&id])
            .expect("delete task");
    }
}

impl TaskStorage for DbConn {
    fn get_task(&self, id: u32) -> Option<Task> {
        let mut stmt = self.0
            .prepare("SELECT id, name, due, hours FROM tasks WHERE id = ?;")
            .expect("select task statement");
        let maybe_task: Result<Task, rusqlite::Error> = stmt.query_row(&[&id], |row| {
            let mut task = Self::task_from_row(row);
            task.tags = self.tags_for_task(task.id);
            task
        });

        match maybe_task {
            Ok(x) => Some(x),
            Err(QueryReturnedNoRows) => None,
            e @ Err(_) => Some(e.expect("query task")),
        }
    }

    fn insert_task(&mut self, task: NewTask) {
        let tx = self.0.transaction().expect("db transaction");
        let id = {
            let mut stmt = tx.prepare("INSERT INTO tasks (name, due, hours) VALUES (?, ?, ?);")
                .expect("prepare insert task");
            stmt.insert(&[&task.name, &task.due, &f64::from(task.hours)])
                .expect("insert task")
        };

        {
            let mut stmt = tx.prepare("INSERT INTO tasks_tags (task, tag) VALUES (?, ?);")
                .expect("prepare insert task tags");
            for tag in task.tags {
                stmt.execute(&[&id, &tag]).expect("insert task tag");
            }
        }

        tx.commit().expect("commit insert task");
    }

    fn put_task(&mut self, task: Task) {
        let tx = self.0.transaction().expect("db transaction");
        Self::delete_task_internal(&tx, task.id);
        tx.execute(
            "INSERT INTO tasks (id, name, due, hours) VALUES (?, ?, ?, ?);",
            &[&task.id, &task.name, &task.due, &f64::from(task.hours)],
        ).expect("insert task");

        {
            let mut stmt = tx.prepare("INSERT INTO tasks_tags (task, tag) VALUES (?, ?);")
                .expect("prepare insert task tags");
            for tag in task.tags {
                stmt.execute(&[&task.id, &tag]).expect("insert task tag");
            }
        }

        tx.commit().expect("commit insert task");
    }

    fn delete_task(&mut self, id: u32) -> Option<Task> {
        let task = self.get_task(id);
        if task.is_some() {
            let tx = self.0.transaction().expect("db transaction");
            Self::delete_task_internal(&tx, id);
            tx.commit().expect("commit delete task");
        }
        task
    }

    fn get_tasks(&self) -> Vec<Task> {
        let mut stmt = self.0
            .prepare("SELECT id, name, due, hours FROM tasks;")
            .expect("select tasks statement");
        let tasks: Result<Vec<Task>, rusqlite::Error> = stmt.query_map(&[], |row| {
            let mut task = Self::task_from_row(row);
            task.tags = self.tags_for_task(task.id);
            task
        }).expect("query tasks")
            .collect();
        tasks.expect("query tasks results")
    }

    fn get_tasks_by_tag(&self, tag: &str) -> Vec<Task> {
        let mut stmt = self.0.prepare("SELECT id, name, due, hours FROM tasks JOIN tasks_tags ON id = tasks_tags.task WHERE tasks_tags.tag = ?;").expect("select tasks statement");
        let tasks: Result<Vec<Task>, rusqlite::Error> = stmt.query_map(&[&tag], |row| {
            let mut task = Self::task_from_row(row);
            task.tags = self.tags_for_task(task.id);
            task
        }).expect("query tasks")
            .collect();
        tasks.expect("query tasks results")
    }
}

const SCHEMA: &str = "
CREATE TABLE IF NOT EXISTS tasks(
  'id' INTEGER PRIMARY KEY,
  'name' TEXT,
  'due' DATE,
  'hours' REAL
);

CREATE TABLE IF NOT EXISTS tasks_tags(
  'task' INTEGER REFERENCES tasks(id),
  'tag' TEXT
);
";
