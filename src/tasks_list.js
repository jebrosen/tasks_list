const TL = {};

TL.today = function() {
	return TL.convertDateTo(new Date(), "array");
};

TL.convertDateTo = function(date, type) {
	if (typeof date === "string") {
		if (type === "string") {
			return date;
		} else if (type === "array") {
			return date.split(/[- ]+/).map(Number);
		} else if (type === "Date") {
			return TL.convertDateTo(TL.convertDateTo(date, "array"), "Date");
		}
	} else if (Array.isArray(date)) {
		if (type === "string") {
			return date[0].toString().padStart(4, "0") + "-" +
				date[1].toString().padStart(2, "0") + "-" +
				date[2].toString().padStart(2, "0");
		} else if (type === "array") {
			return date;
		} else if (type === "Date") {
			return new Date(date[0], date[1]-1, date[2]);
		}
	} else if (date instanceof Date) {
		if (type === "string") {
			return TL.convertDateTo(TL.convertDateTo(date, "array"), "string");
		} else if (type === "array") {
			return [date.getFullYear(), date.getMonth()+1, date.getDate()];
		} else if (type === "Date") {
			return date;
		}
	}
};

TL.compareDates = function(date1, date2) {
	return TL.convertDateTo(date1, "Date") - TL.convertDateTo(date2, "Date");
};

TL.addOneDay = function(date, format) {
	const dateArr = TL.convertDateTo(date, "array");
	dateArr[2] += 1;
	return TL.convertDateTo(TL.convertDateTo(dateArr, "Date"), format);
};

// RGBsooner, RGBlater
// Sun-Sat = ROYGBPR
TL.colorSchemes = [
	[ 0.8, 0.2, 0.2, 1.0, 0.6, 0.6 ],
	[ 1.0, 0.5, 0.0, 1.0, 0.8, 0.0 ],
	[ 0.7, 0.7, 0.0, 1.0, 1.0, 0.0 ],
	[ 0.0, 0.6, 0.0, 0.0, 1.0, 0.0 ],
	[ 0.3, 0.3, 0.8, 0.5, 0.5, 1.0 ],
	[ 0.4, 0.2, 0.6, 0.7, 0.0, 1.0 ],
	[ 0.8, 0.2, 0.2, 1.0, 0.6, 0.6 ],
];

TL.todayScheme = TL.colorSchemes[TL.convertDateTo(TL.today(), "Date").getDay()];

TL.getDateColor = function(date) {
	const millis = TL.compareDates(date, TL.today());
	const maxDays = 4;

	const days = millis / 1000 / 60 / 60 / 24;
	if (days > maxDays) {
		return null;
	}

	const norm = (Math.max(-1, days) + 1) / (maxDays+1);
	const ca = [];
	for (let i = 0; i < 3; i+=1) {
		const c0 = TL.todayScheme[i], c1 = TL.todayScheme[3 + i];
		const start = 255 * c0;
		const range = 255 * (c1 - c0);
		ca[i] = Math.floor(start + norm * range);
	}
	const colorVal = (ca[0] << 16) + (ca[1] << 8) + ca[2];
	return "#" + colorVal.toString(16).padStart(6, "0");
};

class TaskStore {
	getTasks(showAll) {
		let path = "/tasks/";
		if (!showAll) {
			path += "?priority=true";
		}
		return fetch(path).then(function(res) {
			return res.json().then(json => json.tasks);
		});
	}

	addTask(task) {
		return fetch("/tasks/", {
			method: "POST",
			body: JSON.stringify(task),
			headers: { "Content-Type": "application/json" },
		});
	}

	putTask(task) {
		return fetch("/tasks/" + task.id, {
			method: "PUT",
			body: JSON.stringify(task),
			headers: { "Content-Type": "application/json" }
		});
	}

	deleteTask(id) {
		return fetch("/tasks/" + id, { method: "DELETE" });
	}
}

Vue.component("task-table-edit-row", {
	props: ["task", "color"],
	data() {
		return {
			name: this.task.name,
			tags: this.task.tags.join(","),
			due: this.task.due,
			hours: this.task.hours,
		};
	},
	methods: {
		done() {
			const tagsArray = this.tags.split(/[, ]+/);
			const hours = +this.hours;
			const taskObj = { id: this.task.id, name: this.name, tags: tagsArray, due: this.due, hours: hours };
			this.$emit("done", taskObj);
		}
	},
	template: `
		<tr :style="{ backgroundColor: color }">
			<td>{{task.id}}</td>
			<td><input name="name" type="text" size="20" v-model="name" /></td>
			<td><input name="tags" type="text" size="10" v-model="tags" /></td>
			<td><input name="due" type="date" size="10" v-model="due" /></td>
			<td><!-- Postpone --></td>
			<td><input name="hours" type="number" size="2" step="0.01" v-model="hours" /></td>
			<td><!-- Done --></td>
			<td>{{task.value}}</td>
			<td><button @click="done">Done</button></td>
			<td><button @click="$emit('deleted')">Delete</button></td>
		</tr>

	`,
});

Vue.component("task-table-info-row", {
	props: ["task", "color"],
	computed: {
		completed() {
			return this.task.hours <= 0;
		},
	},
	template: `
		<tr :class="{ completed: completed }" :style="{ backgroundColor: color }">
			<td>{{task.id}}</td>
			<td>{{task.name}}</td>
			<td>{{task.tags.join(",")}}</td>
			<td>{{task.due}}</td>
			<td><button @click="$emit('postpone')">+1</button></td>
			<td>{{task.hours}}</td>
			<td><button @click="$emit('complete')">✅</button></td>
			<td>{{task.value}}</td>
			<td><button @click="$emit('edit')">Edit</button></td>
			<td><!-- Delete --></td>
		</tr>
	`,
});

Vue.component("task-table-row", {
	props: ["task"],
	data() {
		return {
			editing: false,
		};
	},
	computed: {
		completed() {
			return this.task.hours <= 0;
		},
		color() {
			return !this.completed && TL.getDateColor(this.task.due);
		}
	},
	methods: {
		postpone() {
			const task = this.task;
			const newDue = TL.addOneDay(task.due, "string");
			const newTask = { id: task.id, name: task.name, tags: task.tags, due: newDue, hours: task.hours };
			this.done(newTask);
		},
		complete() {
			const task = this.task;
			const newTask = { id: task.id, name: task.name, tags: task.tags, due: task.due, hours: 0 };
			this.done(newTask);
		},
		edit() {
			this.editing = true;
		},
		done(newTask) {
			this.$emit("edit", newTask);
			this.editing = false;
		},
	},
	template: `
		<task-table-edit-row v-if="editing" :task="task" :color="color" @done="done" @deleted="$emit('deleted', task.id)"></task-table-edit-row>
		<task-table-info-row v-else :task="task" :color="color" @postpone="postpone" @complete="complete" @edit="edit"></task-table-info-row>
	`,
});

Vue.component("task-total-row", {
	props: ["list"],
	data() {
		return {
			count: 0,
			todayHours: 0,
			totalHours: 0,
		};
	},
	watch: {
		list() {
			const today = TL.today();
			let todayHours = 0, totalHours = 0;
			this.list.forEach(function(t) {
				if (TL.compareDates(t.due, today) <= 0) {
					todayHours += t.hours;
				}
				totalHours += t.hours;
			});

			this.count = this.list.length;
			this.todayHours = todayHours;
			this.totalHours = totalHours;
		}
	},
	template: `
		<tr class="totalRow">
			<td></td>
			<td>{{count}} tasks</td>
			<td><!-- Tags --></td>
			<td><!-- Due --></td>
			<td><!-- Postpone --></td>
			<td>{{todayHours}}/{{totalHours}}</td>
			<td><!-- Done --></td>
			<td><!-- Value --></td>
			<td><!-- Edit --></td>
			<td><!-- Delete --></td>
		</tr>
	`,
});

Vue.component("task-new-row", {
	data() {
		return {
			name: "",
			tags: "",
			due: TL.convertDateTo(TL.today(), "string"),
			hours: 1,
		};
	},
	methods: {
		reset() {
			this.name = "";
			this.tags = "";
			this.due = TL.convertDateTo(TL.today());
			this.hours = 1;
		},
		add() {
			const tagsArray = this.tags.split(/[, ]+/);
			const hours = +this.hours;
			const taskObj = { name: this.name, tags: tagsArray, due: this.due, hours: hours };
			//TODO: reset after added
			this.$emit("add", taskObj);
		}
	},
	template: `
		<tr class="newRow">
			<td>*</td>
			<td><input name="name" type="text" size="20" v-model="name" /></td>
			<td><input name="tags" type="text" size="10" v-model="tags" /></td>
			<td><input name="due" type="date" size="10" v-model="due" /></td>
			<td><!-- Postpone --></td>
			<td><input name="hours" type="number" size="2" step="0.1" v-model="hours" /></td>
			<td><!-- Done --></td>
			<td><!-- Value --></td>
			<td><button @click="add">Add</button></td>
			<td><!-- Delete --></td>
		</tr>
	`,
});

Vue.component("task-table", {
	props: ["store"],
	data() {
		return {
			showAll: false,
			tasks: [],
			error: null,
		};
	},
	created() {
		this.reload();
	},
	watch: {
		showAll() {
			this.reload();
		}
	},
	methods: {
		reload() {
			this.store.getTasks(this.showAll).then(tasks => {
				this.tasks = tasks;
			}).catch(err => {
				this.error = err;
			});
		},
		edit(task) {
			return this.store.putTask(task).then(res => {
				this.reload();
				if (!res.ok) {
					res.text().then(err => {
						this.error = err;
					});
				}
			}).catch(err => {
				this.error = err;
			});
		},
		deleted(id) {
			return this.store.deleteTask(id).then(res => {
				this.reload();
				if (!res.ok) {
					res.text().then(err => {
						this.error = err;
					});
				}
			}).catch(err => {
				this.error = err;
			});
		},
		add(task) {
			return this.store.addTask(task).then(res => {
				this.reload();
				if (!res.ok) {
					res.text().then(err => {
						this.error = err;
					});
				}
			}).catch(err => {
				this.error = err;
			});
		},
	},
	template: `
		<div>
			<h2>Tasks List</h2>
			<label><input name="showAll" type="checkbox" v-model="showAll" /> Show All Tasks</label>
			<table>
				<thead>
					<tr class="header">
						<th>ID</th>
						<th>Task</th>
						<th>Tags</th>
						<th>Due</th>
						<th><!-- Postpone --></th>
						<th>Hours</th>
						<td><!-- Done --></td>
						<th>Value</th>
						<th><!-- Edit --></th>
						<th><!-- Delete --></th>
					</tr>
				</thead>
				<tbody>
					<task-table-row v-for="task in tasks" :key="task.id" :task="task" @edit="edit" @deleted="deleted"></task-table-row>
				</tbody>
				<tfoot>
					<task-total-row :list="tasks"></task-total-row>
					<task-new-row @add="add"></task-new-row>
				</tfoot>
			</table>
			<div>{{error && error.toString()}}</div>
		</div>
	`,
});

new Vue({ el: "#root", data: { store: new TaskStore() } });
