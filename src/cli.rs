use std::borrow::Cow;
use std::error::Error;

use chrono::{Duration, Local, NaiveDate};

use prompt;
use storage::{save, load, TaskStorage};
use task::{Task, NewTask};

pub fn input_loop(store: &mut TaskStorage) {
    if store.get_tasks().len() < 1 {
        store.put_task(Task::new(
            1,
            "Hello world!".to_string(),
            vec!["todo".to_string()],
            Local::today().naive_local(),
            1.0,
        ));
    }

    while let Some(cmd) = prompt_command() {
        if let Err(e) = cmd(store) {
            println!("Error: {}", e);
        }
    }
}

fn input_tags(msg: &str) -> Vec<String> {
    let tags = prompt(msg, None);
    match tags {
        Some(ref tags) if tags != "" => tags.split(',').map(|s| s.trim().into()).collect(),
        _ => vec![],
    }
}

fn input_due_date(msg: &str, allow_empty: bool) -> Option<NaiveDate> {
    loop {
        let input = prompt(msg, None)?;

        if allow_empty && input == "" {
            return None;
        }

        if let Ok(days) = input.parse() {
            let today = Local::today().naive_local();
            return Some(today + Duration::days(days));
        } else if let Ok(date) = NaiveDate::parse_from_str(&input, "%Y-%m-%d") {
            return Some(date);
        } else {
            println!("Please enter date in Y-m-d format or number of days in the future");
        }
    }
}

fn input_hours(msg: &str, allow_empty: bool) -> Option<f32> {
    loop {
        let input = prompt(msg, None)?;

        if allow_empty && input == "" {
            return None;
        }

        if let Ok(hours) = input.parse() {
            return Some(hours);
        } else {
            println!("Hours must be a number")
        }
    }
}

fn truncate(s: &str, width: usize) -> Cow<str> {
    if s.len() > width {
        if width > 3 {
            let mut s = s[..width - 3].to_owned();
            s.push_str("...");
            s.into()
        } else {
            "...".into()
        }
    } else {
        s.into()
    }
}

const HEADER: &str = " ID         Title              Tags           Due    Hrs  Val";
const BORDER: &str = "---+--------------------+----------------+----------+----+---";
fn print_list<'a, I: IntoIterator<Item = &'a Task>>(tasks: I) {
    println!("{}", HEADER);
    println!("{}", BORDER);

    let mut empty = true;
    let mut total_hours = 0.0;
    for task in tasks {
        total_hours += task.hours;
        empty = false;
        println!(
            "{:3} {:20} {:15} {:10} {:4.1} {:3}",
            task.id,
            truncate(&task.name, 20),
            truncate(&task.tags.join(","), 15),
            task.due,
            task.hours,
            task.get_value()
        )
    }

    if empty {
        println!("(No tasks)");
        return;
    }

    println!("{}", BORDER);
    println!(
        "{:3} {:20} {:15} {:10} {:4.1} {:3}",
        "",
        "Total",
        "",
        "",
        total_hours,
        ""
    );
}

type Command = fn(store: &mut TaskStorage) -> Result<(), Box<Error>>;

fn cmd_list(store: &mut TaskStorage) -> Result<(), Box<Error>> {
    print_list(&store.get_tasks());
    Ok(())
}

fn cmd_prilist(store: &mut TaskStorage) -> Result<(), Box<Error>> {
    let mut tasks: Vec<_> = store
        .get_tasks()
        .into_iter()
        .filter(|t| t.hours > 0.0)
        .collect();
    tasks.sort_by_key(|t| t.get_value());
    print_list(&tasks);
    Ok(())
}

fn cmd_find(store: &mut TaskStorage) -> Result<(), Box<Error>> {
    if let Some(tag) = prompt("Tag to find", None) {
        let tasks = store.get_tasks_by_tag(&tag);
        print_list(&tasks);
    }
    Ok(())
}

fn cmd_add(store: &mut TaskStorage) -> Result<(), Box<Error>> {
    fn get_input() -> Option<(String, Vec<String>, NaiveDate, f32)> {
        let name = prompt("Task name", None)?;

        let tags = input_tags("Task tags");
        let due = input_due_date("Task due date", false)?;
        let hours = input_hours("Task hours", false)?;

        Some((name, tags, due, hours))
    }

    if let Some((name, tags, due, hours)) = get_input() {
        store.insert_task(NewTask {
            name,
            tags,
            due,
            hours,
        });
    }

    Ok(())
}

fn cmd_edit(store: &mut TaskStorage) -> Result<(), Box<Error>> {
    let task = loop {
        let which = match prompt("Task number to edit", None) {
            None => return Ok(()),
            Some(ref s) if s == "" => return Ok(()),
            Some(s) => s,
        };

        if let Ok(id) = which.parse() {
            if let Some(t) = store.get_task(id) {
                let id = t.id;
                let name = prompt("Task name", Some(&t.name)).unwrap_or_else(|| t.name.clone());
                let tags = input_tags(&format!("Task tags [{}]", t.tags.join(", ")));
                let tags = if tags.is_empty() {
                    t.tags.clone()
                } else {
                    tags
                };
                let due = input_due_date(&format!("Task due date [{}]", t.due), true)
                    .unwrap_or(t.due);
                let hours = input_hours(&format!("Task hours [{}]", t.hours), true)
                    .unwrap_or(t.hours);
                break Task::new(id, name, tags, due, hours);
            } else {
                println!("No such task")
            }
        } else {
            println!("Invalid task id")
        }
    };


    store.put_task(task);
    Ok(())
}

fn cmd_delete(store: &mut TaskStorage) -> Result<(), Box<Error>> {
    while let Some(id) = prompt("Task number to delete", None) {
        if let Ok(id) = id.parse() {
            let task = store.delete_task(id);
            match task {
                Some(t) => {
                    println!("{:?}", t);
                    break;
                }
                None => {
                    println!("Invalid task");
                }
            }
        }
    }
    Ok(())
}

fn cmd_import(store: &mut TaskStorage) -> Result<(), Box<Error>> {
    if let Some(filename) = prompt("File to import from", None) {
        for task in load(filename)? {
            store.put_task(task);
        }
    };
    Ok(())
}

fn cmd_export(store: &mut TaskStorage) -> Result<(), Box<Error>> {
    if let Some(filename) = prompt("File to export to", None) {
        save(filename, store.get_tasks())?
    };
    Ok(())
}

macro_rules! try_command {
    ($var:ident, $val:expr, $f:ident) => {
        if $var == $val {
            return Some($f)
        }
    }
}

fn prompt_command() -> Option<Command> {
    loop {
        if let Some(line) = prompt("", None) {
            try_command!(line, "l", cmd_list);
            try_command!(line, "p", cmd_prilist);
            try_command!(line, "f", cmd_find);
            try_command!(line, "a", cmd_add);
            try_command!(line, "e", cmd_edit);
            try_command!(line, "d", cmd_delete);
            try_command!(line, "i", cmd_import);
            try_command!(line, "x", cmd_export);

            if line == "" {
                print_help()
            }
            if line == "q" {
                return None;
            }
        } else {
            return None;
        }
    }
}

fn print_help() {
    println!(
        r"[l]ist tasks
[p]riority list tasks
[f]find tasks by tag
[a]dd a task
[e]dit a task
[d]elete a task
e[x]port tasks
[i]mport tasks
[q]uit"
    )
}
