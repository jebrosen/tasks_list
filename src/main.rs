#![feature(plugin)]
#![feature(custom_derive)]
#![plugin(rocket_codegen)]

extern crate chrono;
extern crate r2d2;
extern crate r2d2_sqlite;
extern crate rocket;
extern crate rocket_contrib;
extern crate rusqlite;
extern crate serde;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate serde_json;

mod cli;
mod server;
mod storage;
mod task;

use std::io::{self, Write};

use storage::DbPool;

fn main() {
    let args: Vec<_> = std::env::args().collect();
    let server = args.len() == 2;

    let filename = if server {
        args[1].to_string()
    } else {
        prompt("filename", Some("tasks.sqlite")).expect("filename")
    };

    let pool = DbPool::new(filename).expect("database pool");
    if server {
        server::start(pool)
    } else {
        cli::input_loop(&mut pool.connect().expect("database connection"))
    }
}

fn prompt(msg: &str, default: Option<&str>) -> Option<String> {
    print!("{}", msg);
    if let Some(def) = default {
        print!(" [{}]", def);
    }
    print!(": ");
    io::stdout().flush().expect("flush output");

    let mut input = String::new();
    let bytes = io::stdin().read_line(&mut input).expect("read stdin");
    if bytes == 0 {
        None
    } else if input.trim().is_empty() {
        Some(default.unwrap_or("").into())
    } else {
        Some(input.trim().into())
    }
}
