use chrono::{Local, NaiveDate};

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct Task {
    pub id: u32,
    pub name: String,
    pub tags: Vec<String>,
    pub due: NaiveDate,
    pub hours: f32,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct NewTask {
    pub name: String,
    pub tags: Vec<String>,
    pub due: NaiveDate,
    pub hours: f32,
}

impl Task {
    pub fn new<I: IntoIterator<Item = String>>(
        id: u32,
        name: String,
        tags: I,
        due: NaiveDate,
        hours: f32,
    ) -> Self {
        Self {
            id,
            name: name.into(),
            tags: tags.into_iter().collect(),
            due,
            hours,
        }
    }

    pub fn days_left(&self) -> i64 {
        (self.due.signed_duration_since(Local::today().naive_local())).num_days() + 1
    }

    pub fn get_value(&self) -> i32 {
        (100.0 * self.hours / 2.0_f32.powf((self.days_left() - 1) as f32)).min(1000.0) as i32
    }
}
