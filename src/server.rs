use std::io::Cursor;

use chrono::NaiveDate;
use rocket;
use rocket::Outcome;
use rocket::http::{ContentType, Status};
use rocket::request::{FromRequest, Request, State};
use rocket::response::{Responder, Response};
use rocket::response::status::Custom;
use rocket_contrib::Json;
use serde_json::Value;

use storage::{DbConn, DbPool, TaskStorage};
use task::{Task, NewTask};

pub fn start(pool: DbPool) {
    rocket::ignite()
        .manage(pool)
        .mount(
            "/",
            routes![
            index,
            favicon_ico,
            vue_js,
            tasks_js,
            tasks_get,
            tasks_get_params,
            tasks_post,
            task_get,
            task_put,
            task_delete,
        ],
        )
        .launch();
}

impl<'a, 'r> FromRequest<'a, 'r> for DbConn {
    type Error = ();
    fn from_request(request: &'a Request<'r>) -> rocket::request::Outcome<Self, Self::Error> {
        let conn = State::<DbPool>::from_request(request)?.connect();
        match conn {
            Ok(c) => Outcome::Success(c),
            Err(_) => Outcome::Failure((Status::ServiceUnavailable, ())),
        }
    }
}

const INDEX_HTML: &[u8] = include_bytes!("index.html");
const TASKS_LIST_JS: &[u8] = include_bytes!("tasks_list.js");

#[cfg(debug_assertions)]
const VUE_JS: &[u8] = include_bytes!("vue.js");
#[cfg(not(debug_assertions))]
const VUE_JS: &[u8] = include_bytes!("vue.min.js");

const FAVICON_ICO: &[u8] = include_bytes!("favicon.ico");

struct Content<'a>(&'a [u8], ContentType);
type StaticContent = Content<'static>;

impl<'r> Responder<'r> for Content<'r> {
    fn respond_to(self, _request: &Request) -> Result<Response<'r>, Status> {
        Response::build()
            .header(self.1)
            .sized_body(Cursor::new(self.0))
            .ok()
    }
}

#[get("/")]
fn index() -> StaticContent {
    Content(INDEX_HTML, ContentType::HTML)
}

#[get("/favicon.ico")]
fn favicon_ico() -> StaticContent {
    Content(FAVICON_ICO, ContentType::new("image", "vnd.microsoft.icon"))
}

#[get("/vue.js")]
fn vue_js() -> StaticContent {
    Content(VUE_JS, ContentType::JavaScript)
}

#[get("/tasks_list.js")]
fn tasks_js() -> StaticContent {
    Content(TASKS_LIST_JS, ContentType::JavaScript)
}

#[get("/tasks")]
fn tasks_get(conn: DbConn) -> Json<Value> {
    tasks_get_params(conn, TaskGetParams { priority: None })
}

#[derive(FromForm)]
struct TaskGetParams {
    priority: Option<bool>,
}

#[derive(Clone, Debug, Deserialize, Serialize)]
pub struct ApiTask {
    pub id: u32,
    pub name: String,
    pub tags: Vec<String>,
    pub due: NaiveDate,
    pub hours: f32,
    pub value: i32,
}

impl From<Task> for ApiTask {
    fn from(t: Task) -> Self {
        let value = t.get_value();
        ApiTask {
            id: t.id,
            name: t.name,
            tags: t.tags,
            due: t.due,
            hours: t.hours,
            value,
        }
    }
}

#[get("/tasks?<params>")]
fn tasks_get_params(conn: DbConn, params: TaskGetParams) -> Json<Value> {
    let mut tasks = conn.get_tasks();

    if params.priority == Some(true) {
        tasks = tasks.into_iter().filter(|t| t.hours > 0.0).collect();
        tasks.sort_by_key(|t| -t.get_value());
    }

    Json(
        json!({ "tasks": tasks.into_iter().map(ApiTask::from).collect::<Vec<_>>(), }),
    )
}

#[post("/tasks", data = "<task>")]
fn tasks_post(mut conn: DbConn, task: Json<NewTask>) -> Custom<()> {
    conn.insert_task(task.into_inner());
    Custom(Status::Ok, ())
}

#[get("/tasks/<id>")]
fn task_get(conn: DbConn, id: u32) -> Option<Json<Task>> {
    conn.get_task(id).map(|t| Json(t.clone()))
}

#[put("/tasks/<id>", data = "<task>")]
fn task_put(mut conn: DbConn, id: u32, task: Json<Task>) -> Custom<()> {
    let task = task.into_inner();
    if id == task.id {
        conn.put_task(task);
        Custom(Status::Ok, ())
    } else {
        Custom(Status::BadRequest, ())
    }
}

#[delete("/tasks/<id>")]
fn task_delete(mut conn: DbConn, id: u32) -> Option<Json<Task>> {
    conn.delete_task(id).map(Json)
}
